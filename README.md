# なに

日本語で卒業論文的なものを書くようのテンプレートです。

xetex版とluatex版の2種類用意しました。
特に制約がない場合、luatex版がおすすめです。
Luatexはコンパイルが遅いので、Overleafだと時間切れになることがあります。
その場合は、xetex版を使うのがいいと思います。

## 必要なもの
- Texlive

## luatex版のコンパイル
```
latexmk -lualatex luatex-report.tex
```

## xetex版のコンパイル
```
latexmk -xelatex xetex-report.tex
```

## ライセンス
CC:BY 4.0
